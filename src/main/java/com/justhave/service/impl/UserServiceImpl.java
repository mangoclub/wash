package com.justhave.service.impl;


import com.justhave.context.ContextContainer;
import com.justhave.context.UserContext;
import com.justhave.controller.AjaxBase;
import com.justhave.dao.RoleUserDao;
import com.justhave.dao.UserDao;
import com.justhave.dto.MessageDto;
import com.justhave.dto.UserDto;
import com.justhave.entity.RoleUser;
import com.justhave.entity.User;
import com.justhave.enums.RoleTypeEnums;
import com.justhave.framework.BeanMappingService;
import com.justhave.service.UserService;
import com.justhave.utils.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(AjaxBase.class);

    @Autowired
    private UserDao dao;
    @Autowired
    private RoleUserDao roleUserDao;
    @Autowired
    private BeanMappingService beanMappingService;

    @Override
    @Transactional
    public UserDto login(MessageDto dto, HttpSession httpSession) {

        try {
            MessageDto.checkAndTransfer(dto);

            String openid = dto.getOpenid();
            dto.setOpenid(openid);
            //查看用户是否登录过
            User entity = dao.findByOpenid(openid);
            //更新用户登录记录
            if(entity == null) {
                entity = beanMappingService.transform(dto, User.class);
                EntityUtils.init(entity);
                dao.create(entity);
            } else {
                entity.setCode(dto.getCode());
                EntityUtils.update(entity);
                dao.update(entity);
            }
            //session 的标识
            httpSession.setAttribute("openid", dto.getOpenid());
            logger.info(  "login:success，openid = {}", ContextContainer.getUserContext().getOpenid());
            UserDto userDto = beanMappingService.transform(entity, UserDto.class);
            RoleUser roleUser = roleUserDao.findByOpenid(dto.getOpenid());
            if (roleUser != null) {
                userDto.setRoleType(roleUser.getType());
            } else {
                userDto.setRoleType(RoleTypeEnums.USER.getCode());
            }
            return userDto;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public UserDto findByOpenid(String openid) {
        return beanMappingService.transform(dao.findByOpenid(openid), UserDto.class);
    }

    @Override
    public UserDto getUserInfo() {
        UserContext userContext = ContextContainer.getUserContext();
        return findByOpenid(userContext.getOpenid());
    }

    @Override
    public UserDto update(UserDto dto) {
        dao.update(beanMappingService.transform(dto, User.class));
        return dto;
    }

    @Override
    public UserDto updateContact(UserDto dto) {
        UserContext userContext = ContextContainer.getUserContext();
        User user = User.builder().contact(dto.getContact()).build();
        user.setOpenid(userContext.getOpenid());
        dao.updateContact(user);
        return dto;
    }

}
