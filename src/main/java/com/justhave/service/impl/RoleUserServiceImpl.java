package com.justhave.service.impl;

import com.google.common.collect.Lists;
import com.justhave.common.QueryCondition;
import com.justhave.dao.RoleUserDao;
import com.justhave.dto.RoleUserDto;
import com.justhave.entity.RoleUser;
import com.justhave.enums.RoleTypeEnums;
import com.justhave.framework.BeanMappingService;
import com.justhave.service.RoleUserService;
import com.justhave.utils.AssertUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ajin on 2018/6/17.
 */
@Service
public class RoleUserServiceImpl implements RoleUserService {

    @Autowired
    private RoleUserDao dao;
    @Autowired
    private BeanMappingService beanMappingService;

    @Override
    public RoleTypeEnums getRoleTypeByOpenid(String openid) {
        RoleUser roleUser = dao.findByOpenid(openid);
        if (roleUser == null) return RoleTypeEnums.USER;
        return RoleTypeEnums.getTypeByCode(roleUser.getType());
    }

    @Override
    public List<RoleUserDto> queryByCondition(QueryCondition queryCondition) {
        List<RoleUser> roleUsers  = dao.queryByCondition(queryCondition);
        List<RoleUserDto> dtos = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(roleUsers)) {
            dtos = Lists.transform(roleUsers, roleUser -> beanMappingService.
                    transform(roleUser, RoleUserDto.class));
        }

        return dtos;
    }

    @Override
    public RoleUserDto findById(int id) {
        RoleUser roleUser = dao.findById(id);
        AssertUtils.assertFalse(roleUser == null, "获取员工信息失败");
        RoleUserDto dto = new RoleUserDto();
        BeanUtils.copyProperties(roleUser, dto);
        return dto;
    }
}
