package com.justhave.service.impl;


import com.google.common.collect.Lists;
import com.justhave.common.QueryCondition;
import com.justhave.dao.OrderDao;
import com.justhave.dao.RoleUserDao;
import com.justhave.dao.SelectedListDao;
import com.justhave.dto.MessageDto;
import com.justhave.dto.OrderDto;
import com.justhave.entity.Order;
import com.justhave.entity.RoleUser;
import com.justhave.entity.SelectedList;
import com.justhave.enums.OrderStatusEnums;
import com.justhave.enums.PayTypeEnums;
import com.justhave.framework.BeanMappingService;
import com.justhave.service.MessageService;
import com.justhave.service.OrderService;
import com.justhave.utils.AssertUtils;
import com.justhave.utils.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {


    @Autowired
    private OrderDao dao;
    @Autowired
    private SelectedListDao selectedListDao;
    @Autowired
    private RoleUserDao roleUserDao;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private MessageService messageService;


    @Override
    @Transactional
    public OrderDto create(OrderDto dto, String openid) {
        Order order = beanMappingService.transform(dto, Order.class);
        order.setCustomerOpenid(openid);
        order.setOrderCode(DateUtils.getDateString(new Date(), "yyMMddHHmmss"));
        dao.create(order);
        // create list
        if (dto.getClothInfo() != null) {
            List<SelectedList> selectedList = dto.getClothInfo().getSelectedList();
            if (CollectionUtils.isNotEmpty(selectedList)) {
                selectedList.forEach(c -> {
                    c.setOrderId(order.getId());
                    c.setOpenid(openid);
                });
                selectedListDao.batchCreate(selectedList);
            }
        }

        if (order.getId() > 0) {
            dto.setId(order.getId());
            sendOrderMsg(dto.getMessageData(), order);
        }
        return dto;
    }

    @Override
    @Transactional
    public OrderDto update(OrderDto dto, String openid) {
        Order order = dao.findById(dto.getId());
        OrderStatusEnums statusEnum = OrderStatusEnums.getEnumByCode(dto.getStatus());
        AssertUtils.assertTrue(statusEnum != null && statusEnum.getPreStatusCode() == order.getStatus(),
                "订单已更新");
        order.setStatus(dto.getStatus());
        order.setAccepterOpenid(dto.getAccepterOpenid());
        dao.update(order);

        sendOrderMsg(dto.getMessageData(), order);
        return dto;
    }

    private void sendOrderMsg(MessageDto messageData, Order order) {
        if (messageData != null) {
            messageData.setObjectId(order.getId());
            messageService.send(messageData);
        }
    }

    @Override
    public List<OrderDto> queryByCondition(QueryCondition queryCondition) {
        try {
            List<Order> orders = dao.queryByCondition(queryCondition);
            List<OrderDto> orderDtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(orders)) {
//                orderDtos = Lists.transform(orders, order -> beanMappingService.transform(order, OrderDto.class));
                for(Order order : orders) {
                    OrderDto orderDto = beanMappingService.transform(order, OrderDto.class);
                    generateOrderDto(orderDto);
                    orderDtos.add(orderDto);
                }
            }

            return orderDtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void generateOrderDto(OrderDto orderDto) {
        orderDto.setStatusName(OrderStatusEnums.getDescByCode(orderDto.getStatus()));
        orderDto.setPayTypeName(PayTypeEnums.getDescByCode(orderDto.getPayType()));
        // 接单人委托人信息
        if (StringUtils.isNotEmpty(orderDto.getAccepterOpenid())) {
            RoleUser roleUser = roleUserDao.findByOpenid(orderDto.getAccepterOpenid());
            if (roleUser != null) {
                orderDto.setAccepterName(roleUser.getName());
                orderDto.setAccepterContact(roleUser.getContact());
            }
        } else {
            orderDto.setAccepterName("未接单");
        }
        // customerInfo
        // clothInfo
    }
}
