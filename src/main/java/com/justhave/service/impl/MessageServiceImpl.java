package com.justhave.service.impl;


import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.justhave.common.QueryCondition;
import com.justhave.dao.OrderDao;
import com.justhave.dao.RoleUserDao;
import com.justhave.dao.UserDao;
import com.justhave.dto.MessageData;
import com.justhave.dto.MessageDto;
import com.justhave.dto.OrderDto;
import com.justhave.dto.SendMessageDto;
import com.justhave.entity.ClothInfo;
import com.justhave.entity.CustomerInfo;
import com.justhave.entity.Order;
import com.justhave.entity.RoleUser;
import com.justhave.entity.SelectedList;
import com.justhave.enums.OrderStatusEnums;
import com.justhave.enums.RoleTypeEnums;
import com.justhave.framework.BeanMappingService;
import com.justhave.service.MessageService;
import com.justhave.service.OrderService;
import com.justhave.utils.DateUtils;
import com.justhave.utils.HttpClientUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MessageServiceImpl implements MessageService {


    @Autowired
    private OrderDao orderDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleUserDao roleUserDao;
    @Autowired
    private OrderService orderService;
    @Autowired
    private BeanMappingService beanMappingService;

    public void send(MessageDto dto) {

        int orderId = dto.getObjectId();
        if (orderId > 0) {
            Order order = orderDao.findById(orderId);
            OrderDto orderDto = beanMappingService.transform(order, OrderDto.class);
            orderService.generateOrderDto(orderDto);
            String url = String.format("https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=%s", dto.getAccess_token());
            MessageData data = new MessageData();
            Map<String, String> value1 = Maps.newHashMapWithExpectedSize(2);
            Map<String, String> value2 = Maps.newHashMapWithExpectedSize(2);
            Map<String, String> value3 = Maps.newHashMapWithExpectedSize(2);
            Map<String, String> value4 = Maps.newHashMapWithExpectedSize(2);
            Map<String, String> value5 = Maps.newHashMapWithExpectedSize(2);
            Map<String, String> value6 = Maps.newHashMapWithExpectedSize(2);
            Map<String, String> value7 = Maps.newHashMapWithExpectedSize(2);
            Map<String, String> value8 = Maps.newHashMapWithExpectedSize(2);
            Map<String, String> value9 = Maps.newHashMapWithExpectedSize(2);
            Map<String, String> value10 = Maps.newHashMapWithExpectedSize(2);
            CustomerInfo customerInfo = orderDto.getCustomerInfo();
            ClothInfo clothInfo = orderDto.getClothInfo();
            List<SelectedList> selectedList = clothInfo.getSelectedList();
            value1.put("value", orderDto.getOrderCode());
            value2.put("value", OrderStatusEnums.getDescByCode(orderDto.getStatus()));
            value3.put("value", DateUtils.formatSecond(orderDto.getCreateTime()));
            value4.put("value", customerInfo.getGetDate() + " " + customerInfo.getGetTime());
            value5.put("value", customerInfo.getCustomerName());
            value6.put("value", customerInfo.getCustomerAddress());
            StringBuilder stringBuilder = new StringBuilder("");
            if (CollectionUtils.isNotEmpty(selectedList)) {
                for (SelectedList list : selectedList) {
                    stringBuilder.append(list.getClass_name()).append("(").append(list.getNum()).append("件);");
                }
            }
            value7.put("value", stringBuilder.toString());
            value8.put("value", String.valueOf(clothInfo.getTotalMoney()));
            value9.put("value", customerInfo.getRemark());
            String contact = orderDto.getAccepterContact() == null ? "" : orderDto.getAccepterContact();
            value10.put("value", orderDto.getAccepterName() + " " + contact);
            data.setKeyword1(value1);
            data.setKeyword2(value2);
            data.setKeyword3(value3);
            data.setKeyword4(value4);
            data.setKeyword5(value5);
            data.setKeyword6(value6);
            data.setKeyword7(value7);
            data.setKeyword8(value8);
            data.setKeyword9(value9);
            data.setKeyword10(value10);
            SendMessageDto sendMessageDto = new SendMessageDto(dto.getAccess_token(), dto.getTemplate_id(), dto.getForm_id(),
                    dto.getPage(), orderDto.getCustomerOpenid(), data);
            HttpClientUtils.sendPostRequest(url, null, JSON.toJSONString(sendMessageDto));
            List<String> openids = Lists.newArrayList();
            // 新单据通知员工
            if (orderDto.getStatus() == OrderStatusEnums.WAIT_RECEIVE.getCode()) {
                QueryCondition queryCondition = new QueryCondition();
                queryCondition.setType(RoleTypeEnums.STAFF.getCode());
                List<RoleUser> roleUsers = roleUserDao.queryByCondition(queryCondition);
                if (CollectionUtils.isNotEmpty(roleUsers)) {
                    for (RoleUser roleUser : roleUsers) {
                        if (StringUtils.isNotEmpty(roleUser.getOpenid())) {
                            openids.add(roleUser.getOpenid());
                        }
                    }
                }
            }
            // 通知员工
            if (StringUtils.isNotEmpty(orderDto.getAccepterOpenid())) {
                openids.add(orderDto.getAccepterOpenid());
            }

            if (CollectionUtils.isNotEmpty(openids)) {
                List<MessageDto> formIdDtos = userDao.getFormIds(openids.size());
                if (CollectionUtils.isNotEmpty(formIdDtos)) {
                    List<Integer> formIds = Lists.newArrayListWithExpectedSize(formIdDtos.size());
                    int i = 0;
                    for (MessageDto formIdDto : formIdDtos) {
                        sendMessageDto = new SendMessageDto(dto.getAccess_token(), dto.getTemplate_id(), formIdDto.getForm_id(),
                                dto.getPage(), openids.get(i), data);
                        HttpClientUtils.sendPostRequest(url, null, JSON.toJSONString(sendMessageDto));
                        formIds.add(formIdDto.getId());
                        i ++;
                    }
                    userDao.deleteFormIds(formIds);
                }

            }

        }

    }
}
