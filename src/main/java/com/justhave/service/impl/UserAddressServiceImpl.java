package com.justhave.service.impl;

import com.google.common.collect.Lists;
import com.justhave.common.QueryCondition;
import com.justhave.dao.UserAddressDao;
import com.justhave.dto.UserAddressDto;
import com.justhave.entity.UserAddress;
import com.justhave.framework.BeanMappingService;
import com.justhave.service.UserAddressService;
import com.justhave.utils.AssertUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ajin on 2018/6/17.
 */
@Service
public class UserAddressServiceImpl implements UserAddressService {

    @Autowired
    private UserAddressDao dao;
    @Autowired
    private BeanMappingService beanMappingService;


    @Override
    public List<UserAddressDto> queryByCondition(QueryCondition queryCondition) {
        List<UserAddress> userAddresses  = dao.queryByCondition(queryCondition);
        List<UserAddressDto> dtos = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(userAddresses)) {
            dtos = Lists.transform(userAddresses, userAddress -> beanMappingService.
                    transform(userAddress, UserAddressDto.class));
        }

        return dtos;
    }

    @Override
    public UserAddressDto findById(int id) {
        UserAddress userAddress = dao.findById(id);
        AssertUtils.assertFalse(userAddress == null, "获取地址信息失败");
        UserAddressDto dto = new UserAddressDto();
        BeanUtils.copyProperties(userAddress, dto);
        return dto;
    }
}
