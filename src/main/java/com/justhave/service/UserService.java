package com.justhave.service;


import com.justhave.dto.MessageDto;
import com.justhave.dto.UserDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public interface UserService {

    UserDto login(MessageDto dto, HttpSession httpSession);

    UserDto findByOpenid(String openid);

    UserDto getUserInfo();

    UserDto update(UserDto dto);

    UserDto updateContact(UserDto dto);

}