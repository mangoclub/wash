package com.justhave.service;

import com.justhave.common.QueryCondition;
import com.justhave.dto.UserAddressDto;

import java.util.List;

/**
 * Created by ajin on 2018/6/17.
 */
public interface UserAddressService {

    List<UserAddressDto> queryByCondition(QueryCondition queryCondition);

    UserAddressDto findById(int id);
}
