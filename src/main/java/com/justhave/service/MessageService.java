package com.justhave.service;


import com.justhave.dto.MessageDto;

public interface MessageService {

    void send(MessageDto dto);

}