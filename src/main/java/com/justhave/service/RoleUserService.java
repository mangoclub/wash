package com.justhave.service;

import com.justhave.common.QueryCondition;
import com.justhave.dto.RoleUserDto;
import com.justhave.enums.RoleTypeEnums;

import java.util.List;

/**
 * Created by ajin on 2018/6/17.
 */
public interface RoleUserService {

    RoleTypeEnums getRoleTypeByOpenid(String openid);

    List<RoleUserDto> queryByCondition(QueryCondition queryCondition);

    RoleUserDto findById(int id);
}
