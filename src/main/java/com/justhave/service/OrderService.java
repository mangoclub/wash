package com.justhave.service;


import com.justhave.common.QueryCondition;
import com.justhave.dto.OrderDto;

import java.util.List;

public interface OrderService {

    OrderDto create(OrderDto dto, String openid);

    OrderDto update(OrderDto dto, String openid);

    List<OrderDto> queryByCondition(QueryCondition queryCondition);

    void generateOrderDto(OrderDto orderDto);
}