package com.justhave.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageData {
    private Map<String, String> keyword1;
    private Map<String, String> keyword2;
    private Map<String, String> keyword3;
    private Map<String, String> keyword4;
    private Map<String, String> keyword5;
    private Map<String, String> keyword6;
    private Map<String, String> keyword7;
    private Map<String, String> keyword8;
    private Map<String, String> keyword9;
    private Map<String, String> keyword10;
}