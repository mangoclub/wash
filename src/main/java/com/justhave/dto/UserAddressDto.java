package com.justhave.dto;


import com.justhave.entity.BasicEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAddressDto extends BasicEntity {

	private String name;
	private String sex;
	private String contact;
	private String address;
	private int isDefault;

}
