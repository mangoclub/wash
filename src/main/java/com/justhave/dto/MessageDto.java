package com.justhave.dto;

import com.justhave.utils.AssertUtils;
import com.mysql.jdbc.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageDto {
    private int id;
    private int objectId;

    private String openid;
    private String session_key;
    private String unionid;
    private String code;

    private String appid;
    private String secret;
    private String access_token;
    private String touser;
    private String template_id;
    private String form_id;
    private String page;

    public static void checkAndTransfer(MessageDto dto) {
        AssertUtils.assertFalse(StringUtils.isNullOrEmpty(dto.getOpenid()), "openid为空");
    }

    public static void convert(MessageDto dto) {

    }
}