package com.justhave.dto;


import com.justhave.entity.BasicEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;
import org.codehaus.jackson.annotate.JsonIgnore;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleUserDto extends BasicEntity {

	private int type;
	private int typeName;
	private String name;
	private String sex;
	private String contact;

}
