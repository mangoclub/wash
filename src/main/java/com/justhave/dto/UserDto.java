package com.justhave.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
	private String openid;
	private String nickName;
	private String avatarUrl;
	private String code;
	private String contact;
	private int roleType;


	public static void convert(UserDto dto) {

	}
}
