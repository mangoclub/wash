package com.justhave.dto;


import com.justhave.entity.BasicEntity;
import com.justhave.entity.ClothInfo;
import com.justhave.entity.CustomerInfo;
import com.justhave.utils.AssertUtils;
import com.justhave.utils.CommonUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.regex.Pattern;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto extends BasicEntity {

	private String orderCode;
	private String customerOpenid;
	private String accepterOpenid;
	private String customerName;
	private String accepterName;
	private String accepterContact;
	private CustomerInfo customerInfo;
	private ClothInfo clothInfo;
	private Date acceptTime;
	private Date cancelTime;
	private Date finishTime;
	private int status;
	private String statusName;
	private int payStatus;
	private int payType;
	private String payTypeName;
	private MessageDto messageData;

	private static final Pattern p = Pattern.compile(CommonUtils.phoneRegex);

	public static void check(OrderDto dto) {
		CustomerInfo customerInfo = dto.getCustomerInfo();
		AssertUtils.assertTrue(customerInfo != null,"参数错误");
		AssertUtils.assertTrue(dto.getClothInfo() != null,"参数错误");
		if (dto.getCustomerInfo() != null) {
			CommonUtils.checkStringValid(customerInfo.getCustomerName(), "姓名", false,
					1, 10, null, "");
			CommonUtils.checkStringValid(customerInfo.getCustomerAddress(), "地址", false,
					6, 100, null, "");
			CommonUtils.checkStringValid(customerInfo.getCustomerPhone(), "手机号", false,
					-1, -1, p, "");
			CommonUtils.checkStringValid(customerInfo.getGetDate(), "取衣日期", false,
					-1, -1, null, "");
			CommonUtils.checkStringValid(customerInfo.getGetTime(), "取衣时间", false,
					-1, -1, null, "");
		}
	}
}
