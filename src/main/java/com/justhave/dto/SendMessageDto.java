package com.justhave.dto;

import com.justhave.utils.AssertUtils;
import com.mysql.jdbc.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SendMessageDto {

    private String access_token;
    private String template_id;
    private String form_id;
    private String page;
    private String touser;
    private MessageData data;
}