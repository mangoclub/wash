
package com.justhave.filter;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class SessionFilter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(SessionFilter.class);

	private String excludedPages;
	private String[] excludedPageArray;

	public void init(FilterConfig filterConfig) throws ServletException {
		excludedPages = filterConfig.getInitParameter("excludedPages");
		if (StringUtils.isNotEmpty(excludedPages)) {
			excludedPageArray = excludedPages.split(",");
		}
		return;
	}

	public void doFilter(ServletRequest servletRequest,
			ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {

		boolean isExcludedPage = false;
		for (String page : excludedPageArray) {//判断是否在过滤url之外
			if(((HttpServletRequest) servletRequest).getServletPath().equals(page)){
				isExcludedPage = true;
				break;
			}
		}

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		response.setHeader("Pragma","No-cache");
		response.setHeader("Cache-Control","no-cache");
		response.setDateHeader("Expires",   0);

		if (isExcludedPage) {//在过滤url之外
			chain.doFilter(request, response);
			return;
		}


		String userInfo = (String) request.getSession().getAttribute("userInfo");
		if(StringUtils.isEmpty(userInfo) && !request.getServletPath().contains("/api/")){

			response.sendRedirect(getBasePath(request) + "/");
			return;
		}
		logger.info("method:{},url:{}", request.getMethod(), request.getServletPath());
		chain.doFilter(request, response);
	}

	private String getBasePath(HttpServletRequest request) {
		String path = request.getContextPath();
		String basePath = request.getScheme() + "://"
				+ request.getServerName() + ":" + request.getServerPort()
				+ path;
		return basePath;
	}

	public void destroy() {

		return;
	}

}
