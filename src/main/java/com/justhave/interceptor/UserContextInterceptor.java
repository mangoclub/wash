package com.justhave.interceptor;

import com.justhave.context.ContextContainer;
import com.justhave.context.ContextHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class UserContextInterceptor implements HandlerInterceptor {

    @Autowired
    ContextHandler contextHandler;

    @Override
    public boolean preHandle( HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {

        String openid = (String) httpServletRequest.getSession().getAttribute("openid");
        if(!StringUtils.isEmpty(openid)){
            contextHandler.enrichUserInfo(openid);
        } else{
            throw new LoginException("请重新登陆！");
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        ContextContainer.clear();
    }

}
