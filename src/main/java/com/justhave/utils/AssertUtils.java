package com.justhave.utils;


public class AssertUtils {

    public static void assertEquals(Object actual, Object expected, String msg, Object...args){
        if(expected != null || actual != null) {
            if (expected == null || !expected.equals(actual)) {
                fail(msg,args);
            }
        }
    }

    public static void assertTrue(boolean b, String msg, Object...args){
        if(!b) {
            fail(msg, args);
        }
    }

    public static void assertFalse(boolean b, String msg, Object...args){
        if(b) {
            fail(msg, args);
        }
    }


    public static void assertNull(Object b, String msg, Object...args){
        if(b!=null) {
            fail(msg, args);
        }
    }

    public static void assertNotNull(Object b, String msg, Object...args){
        if(b==null) {
            fail(msg, args);
        }
    }


    public static void fail(String msg, Object...args){
        throw new RuntimeException(String.format(msg,args));
    }


}
