package com.justhave.utils;

import com.justhave.common.QueryCondition;
import com.justhave.enums.OrderStatusEnums;
import com.justhave.enums.RoleTypeEnums;
import com.justhave.service.RoleUserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class ConditionUtils {

    public static QueryCondition generateCondition(HttpServletRequest httpServletRequest, String openid) {
        Map<String, String[]> map = httpServletRequest.getParameterMap();

        int status = map.get("status") == null ? -1 : Integer.valueOf(map.get("status")[0]);
        
        QueryCondition queryCondition = QueryCondition.builder()
                .openid(openid)
                .status(status)
                .current(map.get("current") == null ? null : map.get("current")[0])
                .pageSize(map.get("pageSize") == null ? null : map.get("pageSize")[0])
                .keyword(map.get("keyword") == null ? null : map.get("keyword")[0]).build();

        // 根据角色不同
        RoleTypeEnums roleType = MBeanUtils.getBean(RoleUserService.class).getRoleTypeByOpenid(openid);

        // admin->all staff->self user->self
        if (status == 0) {
            if (roleType != RoleTypeEnums.ADMIN) {
                queryCondition.setCustomerOpenid(openid);
            }
        }

        // admin->all staff->all user->self
        if (status == OrderStatusEnums.WAIT_RECEIVE.getCode()) {
            if (roleType == RoleTypeEnums.USER) {
                queryCondition.setCustomerOpenid(openid);
            }
        }

        // admin->all staff->selfAccept user->no or self
        if (status == OrderStatusEnums.RECEIVED.getCode()) {
            if (roleType == RoleTypeEnums.STAFF) {
                queryCondition.setAccepterOpenid(openid);
            }
            if (roleType == RoleTypeEnums.USER) {
                queryCondition.setCustomerOpenid(openid);
            }
        }

        // admin->all staff->self user->self
        if (status == OrderStatusEnums.SENDING.getCode()) {
            if (roleType == RoleTypeEnums.USER) {
                queryCondition.setCustomerOpenid(openid);
            }
            if (roleType == RoleTypeEnums.STAFF) {
                queryCondition.setAccepterOpenid(openid);
            }
        }

        // admin->all staff->selfAccept user->self
        if (status == OrderStatusEnums.FINISHED.getCode()) {
            if (roleType == RoleTypeEnums.STAFF) {
                queryCondition.setAccepterOpenid(openid);
            }
            if (roleType == RoleTypeEnums.USER) {
                queryCondition.setCustomerOpenid(openid);
            }
        }
        return queryCondition;
    }
}



