package com.justhave.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {

    public static String phoneRegex = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}$";

    public static void checkStringValid(String s, String sName, boolean canEmpty, int minLength, int maxLength,
                                        Pattern p, String pContent) {
        AssertUtils.assertFalse(!canEmpty && StringUtils.isEmpty(s),sName + "不可为空");

        if (StringUtils.isNotEmpty(s)) {
            if (minLength > 0) {
                AssertUtils.assertFalse(s.length() < minLength,sName + "最短为" + minLength);
            }
            if (maxLength > 0) {
                AssertUtils.assertFalse(s.length() > maxLength,sName + "最长为" + maxLength);
            }
            if (p != null) {
                pContent = StringUtils.isEmpty(pContent) ? "" : ("," + pContent);
                AssertUtils.assertFalse(!p.matcher(s).find(), sName + "错误" + pContent);
            }
        }
    }
}
