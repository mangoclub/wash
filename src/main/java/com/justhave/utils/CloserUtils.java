package com.justhave.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;

public class CloserUtils {
    private static final Logger logger = LoggerFactory.getLogger(CloserUtils.class);

    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                logger.error("closer error", e);
            }
        }
    }
}
