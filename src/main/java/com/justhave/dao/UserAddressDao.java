package com.justhave.dao;


import com.justhave.entity.UserAddress;
import org.apache.ibatis.annotations.Param;


public interface UserAddressDao extends BasicDao<UserAddress>{

    int updateDefault(@Param("openid") String openid, @Param("id") int id, @Param("isDefault") int isDefault);

}
