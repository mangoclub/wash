package com.justhave.dao;


import com.justhave.entity.RoleUser;
import com.justhave.entity.User;
import org.apache.ibatis.annotations.Param;


public interface RoleUserDao extends BasicDao<RoleUser>{

    RoleUser findByOpenid(@Param("openid") String openid);

}
