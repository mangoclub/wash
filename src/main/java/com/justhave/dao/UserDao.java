package com.justhave.dao;


import com.justhave.dto.MessageDto;
import com.justhave.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface UserDao extends BasicDao<User>{

    User findByOpenid(@Param("openid") String openid);

    void updateContact(User user);

    void saveFormId(MessageDto dto);

    void deleteFormIds(List<Integer> idList);

    List<MessageDto> getFormIds(@Param("size") int size);

}
