package com.justhave.dao;

import com.justhave.common.QueryCondition;
import com.justhave.entity.BasicEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component("commonBasicDao")
public interface BasicDao<T extends BasicEntity> {
    T findById(Integer id);

    int create(T entity);

    void update(T entity);

    void delete(T entity);

    void batchDeleteByIds(@Param("ids") List<Integer> ids, @Param("updateTime") Date updateTime);

    void batchCreate(List<T> list);

    void batchDelete(List<T> list);

    List<T> queryByCondition(@Param("condition") QueryCondition condition);

}