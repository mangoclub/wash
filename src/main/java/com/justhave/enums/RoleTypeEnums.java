package com.justhave.enums;

public enum RoleTypeEnums {

    ADMIN(1, "商家"),
    STAFF(2, "员工"),
    USER(3, "用户");


    private int code;
    private String desc;

    RoleTypeEnums(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getDescByCode(int code) {
        String ret = null;
        for (RoleTypeEnums enumItem : RoleTypeEnums.values()) {
            if (enumItem.getCode() == code) {
                ret = enumItem.getDesc();
            }
        }
        return ret;
    }

    public static RoleTypeEnums getTypeByCode(int code) {
        RoleTypeEnums ret = RoleTypeEnums.USER;
        for (RoleTypeEnums enumItem : RoleTypeEnums.values()) {
            if (enumItem.getCode() == code) {
                ret = enumItem;
            }
        }
        return ret;
    }
}
