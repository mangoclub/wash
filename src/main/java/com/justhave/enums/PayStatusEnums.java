package com.justhave.enums;

public enum PayStatusEnums {

    NOT_PAY(1, "未支付"),
    HAS_PAY(2, "已支付");


    private int code;
    private String desc;

    PayStatusEnums(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getDescByCode(int code) {
        String ret = null;
        for (PayStatusEnums enumItem : PayStatusEnums.values()) {
            if (enumItem.getCode() == code) {
                ret = enumItem.getDesc();
            }
        }
        return ret;
    }
}
