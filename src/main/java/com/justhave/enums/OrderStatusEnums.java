package com.justhave.enums;

public enum OrderStatusEnums {

    CANCELED(0, 1, "已取消"),
    WAIT_RECEIVE(1, -1, "等待接单"),
    RECEIVED(2, 1, "已接单"),
    SENDING(3, 2, "送件中"),
    FINISHED(4, 3, "已完成");


    private int code;
    private int preStatusCode;
    private String desc;

    OrderStatusEnums(int code, int preStatusCode, String desc) {
        this.code = code;
        this.desc = desc;
        this.preStatusCode = preStatusCode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getPreStatusCode() {
        return preStatusCode;
    }

    public void setPreStatusCode(int preStatusCode) {
        this.preStatusCode = preStatusCode;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getDescByCode(int code) {
        String ret = null;
        for (OrderStatusEnums enumItem : OrderStatusEnums.values()) {
            if (enumItem.getCode() == code) {
                ret = enumItem.getDesc();
            }
        }
        return ret;
    }

    public static OrderStatusEnums getEnumByCode(int code) {
        OrderStatusEnums ret = null;
        for (OrderStatusEnums enumItem : OrderStatusEnums.values()) {
            if (enumItem.getCode() == code) {
                ret = enumItem;
            }
        }
        return ret;
    }
}
