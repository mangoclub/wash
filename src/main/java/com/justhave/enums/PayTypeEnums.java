package com.justhave.enums;

public enum PayTypeEnums {

    CASH(1, "现金支付"),
    WECHAT(2, "微信支付");


    private int code;
    private String desc;

    PayTypeEnums(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getDescByCode(int code) {
        String ret = null;
        for (PayTypeEnums enumItem : PayTypeEnums.values()) {
            if (enumItem.getCode() == code) {
                ret = enumItem.getDesc();
            }
        }
        return ret;
    }
}
