package com.justhave.common;

import com.justhave.context.ContextContainer;
import com.justhave.context.UserContext;
import com.mysql.jdbc.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QueryCondition {

    private String keyword;
    private int start;
    private int limit;
    private String current;
    private String pageSize;
    private int status;
    private String openid;
    private String customerOpenid;
    private String accepterOpenid;
    private int type;

    public static void checkAndTransfer(QueryCondition queryCondition) {
        UserContext userContext = ContextContainer.getUserContext();

        String current = queryCondition.getCurrent();
        String pageSize = queryCondition.getPageSize();

        int start = 0;
        int limit = 10;

        if(!StringUtils.isNullOrEmpty(current) && !StringUtils.isNullOrEmpty(pageSize)) {
            limit = Integer.parseInt(pageSize);
            start = (Integer.parseInt(current) - 1) * limit;
        }

        queryCondition.setStart(start);
        queryCondition.setLimit(limit);
    }
}
