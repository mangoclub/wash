package com.justhave.controller;

import com.justhave.common.QueryCondition;
import com.justhave.common.ResultMap;
import com.justhave.context.ContextContainer;
import com.justhave.dao.RoleUserDao;
import com.justhave.dto.OrderDto;
import com.justhave.entity.RoleUser;
import com.justhave.enums.OrderStatusEnums;
import com.justhave.enums.RoleTypeEnums;
import com.justhave.service.OrderService;
import com.justhave.utils.ConditionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/order")
public class OrderController extends AjaxBase{

    @Autowired
    private RoleUserDao roleUserDao;
    @Autowired
    private OrderService service;


    @PostMapping(value = "/create")
    public ResultMap create(@RequestBody OrderDto dto) {

        try {
            OrderDto.check(dto);
            String openid = ContextContainer.getUserContext().getOpenid();
            return generateRightResultMap("下单成功！", service.create(dto, openid));
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @GetMapping(value = "/all")
    public ResultMap all(HttpServletRequest httpServletRequest) throws Exception{

        try {
            String openid = ContextContainer.getUserContext().getOpenid();
            QueryCondition queryCondition = ConditionUtils.generateCondition(httpServletRequest, openid);
            RoleUser roleUser = roleUserDao.findByOpenid(openid);
            int roleType = RoleTypeEnums.USER.getCode();
            if (roleUser != null) {
                roleType = roleUser.getType();
            }
            return generateRightResultMap(roleType, QUERY_SUCCESS, service.queryByCondition(queryCondition));
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @PostMapping(value = "/cancelOrder")
    public ResultMap cancelOrder(@RequestBody OrderDto dto) {

        try {
            String openid = ContextContainer.getUserContext().getOpenid();
            dto.setStatus(OrderStatusEnums.CANCELED.getCode());
            dto.setAccepterOpenid(openid);
            return generateRightResultMap("取消成功", service.update(dto, openid));
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @PostMapping(value = "/acceptOrder")
    public ResultMap acceptOrder(@RequestBody OrderDto dto) {

        try {
            String openid = ContextContainer.getUserContext().getOpenid();
            dto.setStatus(OrderStatusEnums.RECEIVED.getCode());
            dto.setAccepterOpenid(openid);
            return generateRightResultMap("接单成功", service.update(dto, openid));
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @PostMapping(value = "/finishWash")
    public ResultMap finishWash(@RequestBody OrderDto dto) {

        try {
            String openid = ContextContainer.getUserContext().getOpenid();
            dto.setStatus(OrderStatusEnums.SENDING.getCode());
            dto.setAccepterOpenid(openid);
            return generateRightResultMap("确认成功", service.update(dto, openid));
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @PostMapping(value = "/sendConfirm")
    public ResultMap sendConform(@RequestBody OrderDto dto) {

        try {
            String openid = ContextContainer.getUserContext().getOpenid();
            dto.setStatus(OrderStatusEnums.FINISHED.getCode());
            return generateRightResultMap("确认成功", service.update(dto, openid));
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }
}