package com.justhave.controller;

import com.justhave.common.QueryCondition;
import com.justhave.common.ResultMap;
import com.justhave.context.ContextContainer;
import com.justhave.dao.UserAddressDao;
import com.justhave.dto.UserAddressDto;
import com.justhave.entity.UserAddress;
import com.justhave.service.UserAddressService;
import com.justhave.utils.ConditionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/userAddress")
public class UserAddressController extends AjaxBase{

    @Autowired
    private UserAddressService service;
    @Autowired
    private UserAddressDao dao;


    @PostMapping
    public ResultMap add(@RequestBody UserAddress userAddress) {

        try {
            String openid = ContextContainer.getUserContext().getOpenid();
            userAddress.setOpenid(openid);
            dao.updateDefault(openid, -1, 0);
            int id = dao.create(userAddress);
            return generateRightResultMap(CREATE_SUCCESS, id);
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }

    }

    @PutMapping
    public ResultMap update(@RequestBody UserAddress userAddress) {

        try {
            String openid = ContextContainer.getUserContext().getOpenid();
            UserAddressDto dto = service.findById(userAddress.getId());
            dao.updateDefault(openid, -1, 0);
            dao.update(userAddress);
            return generateRightResultMap(UPDATE_SUCCESS, dto.getId());
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @PostMapping(value = "/updateDefault")
    public ResultMap updateDefault(@RequestBody UserAddress userAddress) {

        try {
            String openid = ContextContainer.getUserContext().getOpenid();
            userAddress.setOpenid(openid);
            int result1 = dao.updateDefault(openid, -1,0);
            int result2 = dao.updateDefault(openid, userAddress.getId(),1);
            return generateRightResultMap(SET_SUCCESS, result2);
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }

    }

    @DeleteMapping
    public ResultMap delete(@RequestBody UserAddress userAddress) {

        try {
            UserAddressDto dto = service.findById(userAddress.getId());
            dao.delete(userAddress);
            return generateRightResultMap(DELETE_SUCCESS, dto.getId());
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @GetMapping(value = "/all")
    public ResultMap all(HttpServletRequest httpServletRequest) throws Exception{

        try {
            String openid = ContextContainer.getUserContext().getOpenid();
            QueryCondition queryCondition = ConditionUtils.generateCondition(httpServletRequest, openid);
            return generateRightResultMap(QUERY_SUCCESS, service.queryByCondition(queryCondition));
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @GetMapping(value = "/getById")
    public ResultMap getOpenid(@RequestParam(value = "id") int id) {

        try {
            return generateRightResultMap(GET_SUCCESS, service.findById(id));
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }

    }
}