package com.justhave.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.justhave.common.ResultMap;
import com.justhave.constant.Constants;
import com.justhave.context.ContextContainer;
import com.justhave.dao.OrderDao;
import com.justhave.dao.UserDao;
import com.justhave.dto.MessageData;
import com.justhave.dto.MessageDto;
import com.justhave.dto.SendMessageDto;
import com.justhave.entity.Order;
import com.justhave.service.UserService;
import com.justhave.utils.AssertUtils;
import com.justhave.utils.HttpClientUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController extends AjaxBase{

    @Autowired
    private UserService service;
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private UserDao dao;

    @PostMapping(value = "/login")
    public ResultMap login(@RequestBody MessageDto dto,
                           HttpSession httpSession) {

        try {
            return generateRightResultMap(LOGIN_SUCCESS,
                    service.login(dto, httpSession));
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }

    }

    @GetMapping(value = "/getOpenid")
    public ResultMap getOpenid(@RequestParam(value = "code") String code,
                               @RequestParam(value = "appid") String appid,
                               @RequestParam(value = "secret") String secret) {

        try {
            String url = String.format("https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=%s",
                    appid, secret, code, Constants.grant_type);
            MessageDto messageDto = JSON.parseObject(HttpClientUtils.sendGetRequest(url), MessageDto.class);
            AssertUtils.assertFalse(StringUtils.isEmpty(messageDto.getOpenid()), "获取用户信息失败");
            String url2 = String.format("https://api.weixin.qq.com/cgi-bin/token?appid=%s&secret=%s&grant_type=%s",
                    appid, secret, "client_credential");
            MessageDto messageDto2 = JSON.parseObject(HttpClientUtils.sendGetRequest(url2), MessageDto.class);
            AssertUtils.assertFalse(StringUtils.isEmpty(messageDto2.getAccess_token()), "获取用户token失败");
            messageDto.setAccess_token(messageDto2.getAccess_token());
            return generateRightResultMap(GET_SUCCESS, messageDto);
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }

    }

    @GetMapping(value = "/getToken")
    public ResultMap getToken(@RequestParam(value = "appid") String appid,
                               @RequestParam(value = "secret") String secret) {

        try {
            String url = String.format("https://api.weixin.qq.com/cgi-bin/token?appid=%s&secret=%s&grant_type=%s",
                    appid, secret, "client_credential");
            MessageDto messageDto = JSON.parseObject(HttpClientUtils.sendGetRequest(url), MessageDto.class);
            AssertUtils.assertFalse(StringUtils.isEmpty(messageDto.getAccess_token()), "获取用户信息失败");
            return generateRightResultMap(GET_SUCCESS, messageDto);
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }

    }

    @PostMapping(value = "/sendMessage")
    public ResultMap sendMessage(@RequestBody MessageDto dto) {

        try {

            int orderId = dto.getObjectId();
            if (orderId > 0) {
                Order order = orderDao.findById(orderId);
                String url = String.format("https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=%s", dto.getAccess_token());
                MessageData data = new MessageData();
                Map<String, String> value1 = Maps.newHashMapWithExpectedSize(2);
                value1.put("value", "test1");
                data.setKeyword1(value1);
                SendMessageDto sendMessageDto = new SendMessageDto(dto.getAccess_token(), dto.getTemplate_id(), dto.getForm_id(),
                        dto.getPage(), order.getCustomerOpenid(), data);

                HttpClientUtils.sendPostRequest(url, null, JSON.toJSONString(sendMessageDto));
            }

            return generateRightResultMap(SEND_SUCCESS, orderId);
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @PostMapping(value = "/saveFormId")
    public ResultMap saveFormId(@RequestBody MessageDto dto) {

        try {
            if (StringUtils.isNotEmpty(dto.getForm_id())) {
                dto.setOpenid(ContextContainer.getUserContext().getOpenid());
                dao.saveFormId(dto);
            }

            return generateRightResultMap(SEND_SUCCESS, dto.getForm_id());
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }
}