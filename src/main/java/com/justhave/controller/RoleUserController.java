package com.justhave.controller;

import com.google.common.collect.Lists;
import com.justhave.common.QueryCondition;
import com.justhave.common.ResultMap;
import com.justhave.context.ContextContainer;
import com.justhave.dao.RoleUserDao;
import com.justhave.dto.RoleUserDto;
import com.justhave.entity.RoleUser;
import com.justhave.enums.RoleTypeEnums;
import com.justhave.service.RoleUserService;
import com.justhave.utils.ConditionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/roleUser")
public class RoleUserController extends AjaxBase{

    @Autowired
    private RoleUserService service;
    @Autowired
    private RoleUserDao dao;


    @PostMapping
    public ResultMap add(@RequestBody RoleUser roleUser) {

        try {
            String openid = ContextContainer.getUserContext().getOpenid();
            roleUser.setOpenid(openid);
            roleUser.setType(RoleTypeEnums.STAFF.getCode());
            if (dao.findByOpenid(openid) != null) {
                throw new Exception("员工已存在!");
            }
            return generateRightResultMap(CREATE_SUCCESS, dao.create(roleUser));
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }

    }

    @PutMapping
    public ResultMap update(@RequestBody RoleUser roleUser) {

        try {
            RoleUserDto dto = service.findById(roleUser.getId());
            dao.update(roleUser);
            return generateRightResultMap(UPDATE_SUCCESS, dto.getId());
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @DeleteMapping
    public ResultMap delete(@RequestBody RoleUser roleUser) {

        try {
            RoleUserDto dto = service.findById(roleUser.getId());
            dao.delete(roleUser);
            return generateRightResultMap(DELETE_SUCCESS, dto.getId());
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @GetMapping(value = "/all")
    public ResultMap all(HttpServletRequest httpServletRequest) throws Exception{

        try {
            String openid = ContextContainer.getUserContext().getOpenid();
            RoleUser roleUser = dao.findByOpenid(openid);
            List<RoleUserDto> roleUserDtos = Lists.newArrayList();
            if (roleUser != null && roleUser.getType() == RoleTypeEnums.ADMIN.getCode()) {
                QueryCondition queryCondition = ConditionUtils.generateCondition(httpServletRequest, openid);
                roleUserDtos =  service.queryByCondition(queryCondition);
            }

            return generateRightResultMap(QUERY_SUCCESS, roleUserDtos);
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }
    }

    @GetMapping(value = "/getById")
    public ResultMap getOpenid(@RequestParam(value = "id") int id) {

        try {
            return generateRightResultMap(GET_SUCCESS, service.findById(id));
        } catch (Exception e) {
            return generateErrorResultMap(e.getMessage());
        }

    }
}