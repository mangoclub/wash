package com.justhave.controller;


import com.google.gson.Gson;
import com.justhave.common.ResultMap;
import com.justhave.context.ContextContainer;
import lombok.extern.log4j.Log4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;


@Controller
@Log4j
public class AjaxBase {

    private static final Logger logger = LoggerFactory.getLogger(AjaxBase.class);

    protected final static int SUCCESS_CODE = 200;
    protected final static int FAILURE_CODE = 400;
    protected final static int SESSIONOUT_CODE = 2000;

    protected final static String SAVE_SUCCESS = "保存成功";
    protected final static String GET_SUCCESS = "获取成功";
    protected final static String REGISTER_SUCCESS = "恭喜你，注册成功！";
    protected final static String QUERY_SUCCESS = "查询成功";
    protected final static String LOGIN_SUCCESS = "登录成功";
    protected final static String LOGOUT_SUCCESS = "退出成功";
    protected final static String CREATE_SUCCESS = "创建成功";
    protected final static String UPDATE_SUCCESS = "更新成功";
    protected final static String SET_SUCCESS = "设置成功";
    protected final static String INSTOCK_SUCCESS = "入库成功";
    protected final static String OUTSTOCK_SUCCESS = "出库成功";
    protected final static String PUBLISH_SUCCESS = "发布成功";
    protected final static String DELETE_SUCCESS = "删除成功";
    protected final static String SUBMIT_SUCCESS = "提交成功";
    protected final static String APPLY_SUCCESS = "申请成功";
    protected final static String APPROVE_SUCCESS = "审批成功";
    protected final static String REJECT_SUCCESS = "驳回成功";
    protected final static String STOP_SUCCESS = "停止成功";
    protected final static String TRANSFER_SUCCESS = "转签成功";
    protected final static String DISTRIBUTE_SUCCESS = "发布成功";
    protected final static String ADD_SUCCESS = "添加成功";
    protected final static String ADD_EMPTY = "请选择添加内容";
    protected final static String CONFIRM_SUCCESS = "确认成功";
    protected final static String CANCEL_SUCCESS = "取消成功";
    protected final static String SEND_SUCCESS = "发送成功";
    protected final static String JOIN_SUCCESS = "参与成功";
    protected final static String CHOOSE_SUCCESS = "选择成功";
    protected final static String OK_SUCCESS = "OK";
    protected final static String FINISH_SUCCESS = "圆满结束";
    public static final String MUL_DELETE_SUCCESS="common_base_deleteSuccess";//删除成功
    public static final String MUL_SAVE_SUCCESS="common_base_saveSuccess";//保存成功
    public static final String MUL_EDIT_SUCCESS="common_base_editSuccess";//编辑成功
    public static final String MUL_PUBLISC_SUCCESS="common_base_publishSuccess";//发布成功

    protected final static String START_SUCCESS = "Enabling success again！";
    protected final static String INACTIVE_SUCCESS = "Deactivate success！";


    protected final static String EDIT_SUCCESS = "编辑成功";
    protected final static String PARAM_ERROR = "参数有误";
    protected final static String PARAM_TIME_ISNULL = "开始和结束时间必填";
    protected final static String DELETE_ERROR = "无权限删除";

    protected final static String DELETE_SKU_SUCCESS = "产品已清空";

    @ExceptionHandler(RuntimeException.class)
    public ModelAndView operateExp(RuntimeException ex, HttpServletRequest request) {
        if (ex instanceof HttpMessageNotReadableException) {
            return new ModelAndView(new View() {
                @Override
                public String getContentType() {
                    return "application/json";
                }

                @Override
                public void render(Map<String, ?> map, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
                    httpServletResponse.setContentType(getContentType());
                    httpServletResponse.setStatus(500);
                    PrintWriter writer = httpServletResponse.getWriter();
                    writer.print(new Gson().toJson(ResultMap.builder().code(500).msg("页面输入错误")));
                    writer.flush();
                }
            });
        }
        throw ex;
    }

    protected ResultMap generateErrorResultMap(String msg) {
        logger.error("fail:" + msg + ",openid = {}", ContextContainer.getUserContext().getOpenid());
        return ResultMap.builder()
                .ret(false)
                .code(FAILURE_CODE)
                .msg(msg)
                .build();
    }

    protected ResultMap generateRightResultMap(String msg, Object data) {
        logger.info("success:" + msg + ",openid = {}", ContextContainer.getUserContext().getOpenid());
        return ResultMap.builder()
                .ret(true)
                .msg(msg)
                .code(SUCCESS_CODE)
                .data(data).build();
    }

    protected ResultMap generateRightResultMap(int roleType,String msg, Object data) {
        logger.info("success:" + msg + ",openid = {}", ContextContainer.getUserContext().getOpenid());
        return ResultMap.builder()
                .ret(true)
                .roleType(roleType)
                .msg(msg)
                .code(SUCCESS_CODE)
                .data(data).build();
    }

    protected ResultMap generateResultMap(boolean ret, int code, String msg, Object data) {
        logger.info(msg + ",openid = {}", ContextContainer.getUserContext().getOpenid());
        return ResultMap.builder().ret(ret).code(code).msg(msg).data(data).build();
    }

}
