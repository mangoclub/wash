package com.justhave.framework;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


public class BeanMappingServiceImpl implements BeanMappingService {

    @Autowired
    @Qualifier("adminBeanMapper")
    private Mapper mapper;

    public <T> T transform(Object o, Class<T> clazz) {
        try {
            T t =clazz.newInstance();
            if (o == null) return null;
            mapper.map(o,t);
            return t;
        }   catch (Exception e){
            throw new RuntimeException("您的操作有误！",e);
        }
    }
}
