package com.justhave.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order extends BasicEntity {

	private String orderCode;
	private String customerOpenid;
	private String accepterOpenid;
	private CustomerInfo customerInfo;
	private ClothInfo clothInfo;
	private Date acceptTime;
	private Date cancelTime;
	private Date finishTime;
	private int status;
	private int payStatus;
	private int payType;
}
