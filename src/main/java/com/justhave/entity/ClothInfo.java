package com.justhave.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClothInfo extends BasicEntity{
	private int totalCount;
	private double money;
	private double privilege;
	private double freight;
	private double totalMoney;

	private List<SelectedList> selectedList;
}
