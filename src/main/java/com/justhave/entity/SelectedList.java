package com.justhave.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SelectedList extends BasicEntity{
	private int orderId;
	private int pid;
	private int idx;
	private String class_name;
	private int num;
	private double price;
}
