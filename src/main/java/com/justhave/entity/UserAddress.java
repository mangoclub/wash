package com.justhave.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAddress extends BasicEntity {

	private String name;
	private String sex;
	private String contact;
	private String address;
	private int isDefault;
}
