package com.justhave.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleUser extends BasicEntity {

	private int type;
	private String name;
	private String sex;
	private String contact;
	private String password;

}
