package com.justhave.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User extends BasicEntity {

	private String nickName;
	private String avatarUrl;
	private String code;
	private String contact;
}
