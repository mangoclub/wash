package com.justhave.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerInfo extends BasicEntity{
	private String customerName;
	private String customerPhone;
	private String customerAddress;
	private String getDate;
	private String getTime;
	private String remark;
}
