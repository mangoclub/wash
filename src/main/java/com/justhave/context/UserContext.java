package com.justhave.context;

import com.justhave.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserContext {
    private int shopId;
    private int userId;
    private String accountName;
    private String imageUrl;
    private String languageCode="0001";
    private String ip;
    private String userAgent;

    private UserDto wxUserDto;
    private String openid;

}
