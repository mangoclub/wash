package com.justhave.context;


import com.justhave.dto.UserDto;
import com.justhave.service.UserService;
import com.justhave.utils.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;


@Component
public class ContextHandler {

    @Autowired
    private UserService wxUserService;



    public void enrichUserInfo(String info) throws ExecutionException {
        UserContext u = ContextContainer.getUserContext();
        enrichUserInfo(info, u);

    }


    private void enrichUserInfo(String info, UserContext u) {

        UserDto wxUserDto = wxUserService.findByOpenid(info);

        if(wxUserDto!=null){
            u.setOpenid(info);
            u.setWxUserDto(wxUserDto);
        }else {
            throw new ApplicationException("获取信息失败");
        }
    }

}
