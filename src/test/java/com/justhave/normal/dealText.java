package com.justhave.normal;

import com.google.common.base.Strings;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by jane on 2017/7/1.
 */
public class dealText {
    /**
     * 读取txt文件的内容
     * @param file 想要读取的文件对象
     * @return 返回文件内容
     */
    public static String toSelect(File file){
        StringBuilder result = new StringBuilder();
        try{
            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
            String s = null;
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                result.append("t."+s+","+System.lineSeparator());
            }
            br.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return result.toString();
    }

    public static String toCreate(File file){
        StringBuilder result = new StringBuilder();
        try{
            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
            String s = null;
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                if(!s.equals("id") && !s.contains("extend") && !Strings.isNullOrEmpty(s)) {
                    result.append(s+","+System.lineSeparator());
                }
            }
            br.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return result.toString();
    }
    public static String toCreate2(File file){
        StringBuilder result = new StringBuilder();
        try{
            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
            String s = null;
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                if(!s.equals("id") && !s.contains("extend") && !Strings.isNullOrEmpty(s)) {
                    result.append("#{"+s+"},"+System.lineSeparator());
                }
            }
            br.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return result.toString();
    }

    public static String toUpdate(File file){
        StringBuilder result = new StringBuilder();
        try{
            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
            String s = null;
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                if(!s.equals("id") && !s.contains("extend") && !Strings.isNullOrEmpty(s.trim())) {
                    result.append(s+" = "+"#{"+s+"},"+System.lineSeparator());
                }
            }
            br.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return result.toString();
    }
    public static void main(String[] args){
        File file = new File("/Users/jane/jane/tempDoc/mysql.txt");
        System.out.println("select*******\n\n");
        System.out.println(toSelect(file));
        System.out.println("crete********\n\n");
        System.out.println(toCreate(file));
        System.out.println(toCreate2(file));
        System.out.println("update*******\n\n");
        System.out.println(toUpdate(file));
    }
}
