package com.justhave.normal;

import org.apache.log4j.Logger;
import org.apache.log4j.net.SMTPAppender;

public class TestLog4jSendMail {
    static Logger logger = Logger.getLogger(TestLog4jSendMail.class);
    SMTPAppender appender = new SMTPAppender();

    public TestLog4jSendMail() {
        try {
            appender.setSMTPUsername("believemango@163.com");
            appender.setSMTPPassword("jack8339811");
            appender.setTo("believemango@163.com");
            appender.setFrom("believemango@163.com");
            // SMTP服务器 smtp.163.com
            appender.setSMTPHost("smtp.163.com");
            appender.setLocationInfo(true);
            appender.setSubject("Test Mail From Log4J");
            appender.setLayout(new DefineLayOut());
            appender.activateOptions();
            logger.addAppender(appender);
            logger.error("测试");
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Printing ERROR Statements", e);
        }
    }

    public static void main(String args[]) {
        new TestLog4jSendMail();
    }
}