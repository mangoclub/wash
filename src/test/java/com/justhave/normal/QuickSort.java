package com.justhave.normal;

/**
 * Created by jane on 2017/9/16.
 */
public class QuickSort {



    public static void main(String args[]) {
        int[] arr = {2, 44, 11, 9, 7, 5, 4, 3, 1, 33};
        // int[] arr = {2, 5, 4, 3, 1};
        sort(arr, 0, arr.length - 1);
        print(arr);
    }


    public static void sort(int[] a, int low, int high) {
        if(low >= high) return;
        if((high - low ) == 1) {
            if(a[low] > a[high]) {
                swap(a, low, high);
            }
            return;
        }

        int pivot = a[low];
        int left = low + 1;
        int right = high;
        while (left < right) {
            while (left < right && left <= high) {
                if(a[left] > pivot) break;
                left ++;

            }

            while (left <= right && right > low) {
                if(a[right] < pivot) break;
                right --;
            }
            if(left < right) {
                swap(a, right, left);
            }
            System.out.println(left + ":" + right);
            print(a);
        }

        swap(a, low, right);
        //System.out.print("a:");
        //print(a);
        sort(a, low, right);
        //System.out.print("b:");
        //print(a);
        sort(a, right + 1, high);
        //System.out.print("c:");
        //print(a);

    }

    private static void swap(int[] array, int i, int j) {
//        int temp;
//        temp = array[i];
//        array[i] = array[j];
//        array[j] = temp;

        if(i != j) {
            array[i] = array[i] ^ array[j];
            array[j] = array[i] ^ array[j];
            array[i] = array[i] ^ array[j];
        }
    }


    public static void print(int[] before) {
        for(int i = 0; i < before.length; i ++) {
            System.out.print(before[i] + " ");
        }
        System.out.println();
    }

    // 2 5 4 3 1
    // 2
    // 5>2 left = 1
    // 1<2 right = 4
    // 2 1 4 3 5
    // 4>2 left = 2
    // 1<2 right = 1
    // 1 2 4 3 5
    // 4 3 5
    // 4
    // 5>4 left = 4
    // 3<4 right = 3
    // low = 2
    // 1 2 3 4 5

    // 2(0), 44(1), 11(2), 9(3), 7(4), 5(5), 4(6), 3(7), 1(8), 33(9)
    // pivot = 2
    // low = 0
    // 44>2 left = 1
    // 1<2 right = 8
    // left < right
    // 2(0), 1(1), 11(2), 9(3), 7(4), 5(5), 4(6), 3(7), 44(8), 33(9)
    // 11>2 left = 2
    // 1<2 right = 1
    // 1(0), 2(1), 11(2), 9(3), 7(4), 5(5), 4(6), 3(7), 44(8), 33(9)

    // 11(2), 9(3), 7(4), 5(5), 4(6), 3(7), 44(8), 33(9)
    // pivot = 11
    // low = 2
    // 44>11 left = 8
    // 2<11 right = 7
    // 3(2), 9(3), 7(4), 5(5), 4(6), 11(7), 44(8), 33(9)
    // sort(2 ~ 7)
    // 3(2), 9(3), 7(4), 5(5), 4(6), 11(7)
    // sort(8 ~ 9)
    // 44(8), 33(9)
    // pivot = 44
    // low = 8

}
