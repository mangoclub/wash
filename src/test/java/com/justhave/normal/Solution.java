package com.justhave.normal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Created by jane on 2017/9/18.
 */
class Solution {
//    public List<List<Integer>> threeSum(int[] num) {
//        Arrays.sort(num);
//        List<List<Integer>> res = new LinkedList<>();
//        for (int i = 0; i < num.length-2; i++) {
//            if (i == 0 || (i > 0 && num[i] != num[i-1])) {
//                int lo = i+1, hi = num.length-1, sum = 0 - num[i];
//                while (lo < hi) {
//                    if (num[lo] + num[hi] == sum) {
//                        res.add(Arrays.asList(num[i], num[lo], num[hi]));
//                        while (lo < hi && num[lo] == num[lo+1]) lo++;
//                        while (lo < hi && num[hi] == num[hi-1]) hi--;
//                        lo++; hi--;
//                    } else if (num[lo] + num[hi] < sum) lo++;
//                    else hi--;
//                }
//            }
//        }
//        return res;
//    }
    public static List<List<Integer>> threeSum(int[] nums) {

        List<List<Integer>> list = new ArrayList<List<Integer>>();
        int [] newData;
        for(int i = 0; i < nums.length; i++) {
            for(int j = i + 1; j < nums.length; j++) {
                    newData = Arrays.copyOfRange(nums, j+1, nums.length);
                    int wait = 0-(nums[i] + nums[j]);
                    if(Arrays.asList(newData).contains(wait)) {
                        List<Integer> l = new ArrayList<Integer>();
                        l.add(nums[i]);
                        l.add(nums[j]);
                        l.add(wait);

                        if(checked(l, list)) {
                            list.add(l);
                        }


                    }
            }
        }

        return list;
    }

    private static boolean checked(List<Integer> l, List<List<Integer>> list) {
        boolean flag = true;
        if(list != null && list.size() > 0) {
            for(List<Integer> li : list) {
                if(l.get(0) == 0 && l.get(1) == 0) {
                    if(li.get(0) == 0 && li.get(2) ==0) return false;
                } else {
                    if(li.contains(l.get(0)) && li.contains(l.get(1)) && li.contains(l.get(2))) return false;
                }

            }
        }
        return flag;
    }

    public static void main(String[] args) {
        int[] nums = {-4,-2,1,-5,-4,-4,4,-2,0,4,0,-2,3,1,-5,0};
        System.out.println(threeSum(nums));
        String a = "1+2i";
        String b = a;
        String s = ((Function<int[], String>) vs -> (vs[0] * vs[2] - vs[1] * vs[3]) + "+" + (vs[1] * vs[2] + vs[0] * vs[3]) + "i").apply(Arrays.stream((a + b).split("[+i]")).mapToInt(Integer::parseInt).toArray());
        System.out.println(Stream.of(a.split("\\+|i")).mapToInt(Integer::parseInt).toArray()[0]);
        System.out.println(s);
    }

}
