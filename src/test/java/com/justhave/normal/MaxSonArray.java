package com.justhave.normal;


import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 * Created by jane on 2017/9/18.
 */
class MaxSonArray {

    public static void main(String[] args) {
        int[] a = {1, -2, 3, 5, -3, 2};
        int sum=0;
        //其实要处理全是负数的情况，很简单，如稍后下面第3点所见，直接把这句改成："int sum=a[0]"即可
        //也可以不改，当全是负数的情况，直接返回0，也不见得不行。
        int b=0;

        for(int i=0; i<a.length; i++)
        {
            if(b<0)           //...
                b=a[i];
            else
                b+=a[i];
            if(sum<b)
                sum=b;
        }

        System.out.println(sum);
    }

}
