CREATE DATABASE  IF NOT EXISTS `justhave` ;
USE `justhave`;

CREATE TABLE `wx_t_user` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `openid` varchar(100) DEFAULT NULL COMMENT '用户唯一标识',
  `avatarUrl` varchar(200) DEFAULT NULL COMMENT '头像',
  `nickName` varchar(50) DEFAULT NULL COMMENT '昵称',
  `code` varchar(50) DEFAULT NULL COMMENT '登录code',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `ip` varchar(30) DEFAULT NULL,
  `valid` tinyint(4) DEFAULT NULL,
  `extend1` varchar(50) DEFAULT NULL COMMENT '我发布的次数(发布+1)',
  `extend2` varchar(50) DEFAULT NULL COMMENT '我参与的次数(确认+1)',
  `extend3` varchar(50) DEFAULT NULL COMMENT '我的好评次数',
  `extend4` varchar(50) DEFAULT NULL COMMENT '我的中评次数',
  `extend5` varchar(50) DEFAULT NULL COMMENT '我的差评次数',
  `extend6` varchar(50) DEFAULT NULL COMMENT '最近登陆时间',
  `extend7` varchar(50) DEFAULT NULL,
  `extend8` varchar(50) DEFAULT NULL,
  `extend9` varchar(50) DEFAULT NULL,
  `extend10` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户表';

CREATE TABLE `wx_t_task` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(100) DEFAULT NULL COMMENT '标题',
  `content` varchar(500) DEFAULT NULL COMMENT '内容',
  `contact` varchar(50) DEFAULT NULL COMMENT '发布者联系方式',
  `remark` varchar(300) DEFAULT NULL COMMENT '备注',
  `publishOpenid` varchar(100) DEFAULT NULL COMMENT '发布者openid',
  `publishLongitude` decimal(19,6) DEFAULT NULL COMMENT '发布者经度',
  `publishLatitude` decimal(19,6) DEFAULT NULL COMMENT '发布者纬度',
  `targetAddress` varchar(100) DEFAULT NULL COMMENT '目标地址',
  `targetName` varchar(100) DEFAULT NULL COMMENT '目标名称',
  `targetLongitude` decimal(19,6) DEFAULT NULL COMMENT '目标经度',
  `targetLatitude` decimal(19,6) DEFAULT NULL COMMENT '目标纬度',
  `accepterOpenid` varchar(100) DEFAULT NULL COMMENT '接受者openid',
  `accepterContact` varchar(50) DEFAULT NULL COMMENT '接受者联系方式',
  `accepterLongitude` decimal(19,6) DEFAULT NULL COMMENT '接受者经度',
  `accepterLatitude` decimal(19,6) DEFAULT NULL COMMENT '接受者纬度',
  `status` varchar(5) DEFAULT NULL COMMENT '状态：0 待领取 1 进行中 2 完成待确认 3 结束',
  `commission` varchar(10) DEFAULT NULL COMMENT '佣金',
  `type` varchar(5) DEFAULT NULL COMMENT '任务类型',
  `viewCount` int(10) DEFAULT NULL COMMENT '查看次数',
  `publishTime` datetime DEFAULT NULL COMMENT '发布时间',
  `acceptTime` datetime DEFAULT NULL COMMENT '接受时间',
  `finishTime` datetime DEFAULT NULL COMMENT '完成时间',
  `confirmTime` datetime DEFAULT NULL COMMENT '确认时间',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `ip` varchar(30) DEFAULT NULL,
  `valid` tinyint(4) DEFAULT NULL,
  `extend1` varchar(50) DEFAULT NULL COMMENT '发布者的评分 1 好评 2 中评 3 差评',
  `extend2` varchar(50) DEFAULT NULL COMMENT '接受者的评分',
  `extend3` varchar(50) DEFAULT NULL COMMENT '发布者的评分时间',
  `extend4` varchar(50) DEFAULT NULL COMMENT '接受者的评分时间',
  `extend5` varchar(50) DEFAULT NULL COMMENT '期望完成时间',
  `extend6` varchar(50) DEFAULT NULL,
  `extend7` varchar(50) DEFAULT NULL,
  `extend8` varchar(50) DEFAULT NULL,
  `extend9` varchar(50) DEFAULT NULL,
  `extend10` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='任务表';

CREATE TABLE `common_t_distributedLock` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `expiredTime` datetime DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `valid` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `common_t_distributedLock_key_uindex` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='锁表';

CREATE TABLE `wx_tr_jointask` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `taskId` bigint(10) NOT NULL COMMENT 'taskID',
  `myopenid` varchar(100) DEFAULT NULL COMMENT '发布者openid',
  `openid` varchar(100) DEFAULT NULL COMMENT '接受者openid',
  `contact` varchar(50) DEFAULT NULL COMMENT '接受者联系方式',
  `longitude` decimal(19,6) DEFAULT NULL COMMENT '接受者经度',
  `latitude` decimal(19,6) DEFAULT NULL COMMENT '接受者纬度',
  `joined` varchar(5) DEFAULT NULL COMMENT '接受者状态：0 等待中 1 被选中',
  `status` varchar(5) DEFAULT NULL COMMENT '任务状态：0 挑选中 1 已挑选',
  `joinTime` datetime DEFAULT NULL COMMENT '加入时间',
  `acceptTime` datetime DEFAULT NULL COMMENT '接受时间',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `ip` varchar(30) DEFAULT NULL,
  `valid` tinyint(4) DEFAULT NULL,
  `extend1` varchar(50) DEFAULT NULL,
  `extend2` varchar(50) DEFAULT NULL,
  `extend3` varchar(50) DEFAULT NULL,
  `extend4` varchar(50) DEFAULT NULL,
  `extend5` varchar(50) DEFAULT NULL,
  `extend6` varchar(50) DEFAULT NULL,
  `extend7` varchar(50) DEFAULT NULL,
  `extend8` varchar(50) DEFAULT NULL,
  `extend9` varchar(50) DEFAULT NULL,
  `extend10` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='挑选人中间表';

CREATE TABLE `wx_t_daily` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `nickName` varchar(50) DEFAULT NULL COMMENT '昵称',
  `openid` varchar(100) DEFAULT NULL COMMENT 'openid',
  `type` INT(11) DEFAULT NULL COMMENT '日志类型',
  `direction` varchar(255) DEFAULT NULL COMMENT '说明',
  `ip` varchar(30) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `extend1` varchar(50) DEFAULT NULL,
  `extend2` varchar(50) DEFAULT NULL,
  `extend3` varchar(50) DEFAULT NULL,
  `extend4` varchar(50) DEFAULT NULL,
  `extend5` varchar(50) DEFAULT NULL,
  `extend6` varchar(50) DEFAULT NULL,
  `extend7` varchar(50) DEFAULT NULL,
  `extend8` varchar(50) DEFAULT NULL,
  `extend9` varchar(50) DEFAULT NULL,
  `extend10` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='日志记录';

CREATE TABLE `wx_t_report` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `openid` varchar(100) DEFAULT NULL COMMENT 'openid',
  `content` varchar(255) DEFAULT NULL COMMENT '说明',
  `ip` varchar(30) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `extend1` varchar(50) DEFAULT NULL,
  `extend2` varchar(50) DEFAULT NULL,
  `extend3` varchar(50) DEFAULT NULL COMMENT '我的好评次数(显示用)',
  `extend4` varchar(50) DEFAULT NULL COMMENT '我的中评次数(显示用)',
  `extend5` varchar(50) DEFAULT NULL COMMENT '我的差评次数(显示用)',
  `extend6` varchar(50) DEFAULT NULL,
  `extend7` varchar(50) DEFAULT NULL,
  `extend8` varchar(50) DEFAULT NULL,
  `extend9` varchar(50) DEFAULT NULL,
  `extend10` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='投诉记录';



