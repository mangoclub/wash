CREATE DATABASE IF NOT EXISTS `wash` DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
USE `wash`;
-- create user 'washer'@'%' identified by 'wash666';
-- grant all privileges on `wash`.* to 'washer'@'%' identified by 'wash666';
-- alter table t_order drop `updateTime`;
-- alter table t_order add column `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP after createTime;
-- alter table t_role_user drop `updateTime`;
-- alter table t_role_user add column `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP after createTime;
-- alter table t_user_address drop `updateTime`;
-- alter table t_user_address add column `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP after createTime;
CREATE TABLE `t_user` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `openid` varchar(100) DEFAULT NULL COMMENT '用户唯一标识',
  `avatarUrl` varchar(200) DEFAULT NULL COMMENT '头像',
  `nickName` varchar(50) DEFAULT NULL COMMENT '昵称',
  `code` varchar(50) DEFAULT NULL COMMENT '登录code',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否作废0: 未删除 1: 删除',
  PRIMARY KEY (`id`),
  KEY `openid` (`openid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户表';

CREATE TABLE `t_order` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `orderCode` varchar(100) DEFAULT '' COMMENT 'order编码',
  `customerOpenid` varchar(100) DEFAULT '' COMMENT '客户openid',
  `accepterOpenid` varchar(100) DEFAULT '' COMMENT '接单人openid',
  `customerName` varchar(45) DEFAULT NULL COMMENT '客户姓名',
  `customerPhone` varchar(45) DEFAULT '' COMMENT '客户手机',
  `customerAddress` varchar(100) DEFAULT NULL COMMENT '客户地址',
  `getDate` varchar(20) DEFAULT NULL COMMENT '取衣日期',
  `getTime` varchar(20) DEFAULT NULL COMMENT '取衣事件',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `totalCount` INT(11) DEFAULT '0' COMMENT '衣服数量',
  `money` decimal(19,2) DEFAULT '0.00' COMMENT '衣服价格',
  `privilege` decimal(19,2) DEFAULT '0.00' COMMENT '优惠',
  `freight` decimal(19,2) DEFAULT '0.00' COMMENT '运费',
  `totalMoney` decimal(19,2) DEFAULT '0.00' COMMENT '总额',
  `cancelTime` datetime DEFAULT NULL COMMENT '取消时间',
  `acceptTime` datetime DEFAULT NULL COMMENT '接受时间',
  `finishTime` datetime DEFAULT NULL COMMENT '完成时间',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) DEFAULT NULL,
  `payStatus` tinyint(4) DEFAULT NULL,
  `payType` tinyint(4) DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否作废0: 未删除 1: 删除',
  PRIMARY KEY (`id`),
  KEY `customerOpenid` (`customerOpenid`),
  KEY `accepterOpenid` (`accepterOpenid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='订单表';

CREATE TABLE `tr_order_item` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `orderId` bigint(10) NOT NULL COMMENT '订单id',
  `pid` INT(11) DEFAULT '0' COMMENT '父类id',
  `idx` INT(11) DEFAULT '0' COMMENT '自己',
  `num` INT(11) DEFAULT '0' COMMENT '衣服数量',
  `class_name` varchar(100) DEFAULT '' COMMENT '当时的名称',
  `price` decimal(19,2) DEFAULT '0.00' COMMENT '衣服价格',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `openid` varchar(100) DEFAULT NULL COMMENT '用户唯一标识',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否作废0: 未删除 1: 删除',
  PRIMARY KEY (`id`),
  KEY `openid` (`openid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='订单商品表';

CREATE TABLE `t_role_user` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `openid` varchar(100) DEFAULT NULL COMMENT '用户唯一标识',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '员工类型',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(20) DEFAULT NULL COMMENT '性别',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `password` varchar(50) DEFAULT NULL COMMENT '性别',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否作废0: 未删除 1: 删除',
  PRIMARY KEY (`id`),
  KEY `openid` (`openid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='员工表';

CREATE TABLE `t_user_address` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `openid` varchar(100) DEFAULT NULL COMMENT '用户唯一标识',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(20) DEFAULT NULL COMMENT '性别',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `isDefault` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1默认',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否作废0: 未删除 1: 删除',
  PRIMARY KEY (`id`),
  KEY `openid` (`openid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='地址表';

CREATE TABLE `t_form_id` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `openid` varchar(100) DEFAULT NULL COMMENT '用户唯一标识',
  `formid` varchar(50) DEFAULT NULL COMMENT 'formid',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否作废0: 未删除 1: 删除',
  PRIMARY KEY (`id`),
  KEY `isDeleted` (`isDeleted`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='formId';


